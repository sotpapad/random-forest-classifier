%% Random Forest Classifier Training Algorithm

% Input:
%       X_categorical: IxM array of categorical values; rows correspond to
%                       samples and columns to variables
%       X_continuous: IxK array of continuous values; rows correspond to
%                       samples and columns to variables
%       Y: Ix1 vector, containing the target class variable
%       ntree: the number of trees to grow
% Output:
%       model: all neccessary values for new samples classification
%               1st cell = ntrees x models for MinLeafSize==1
%               2st cell = ntrees x models for MinLeafSize==5
%               3rd cell = indexes of samples of training set used in each
%                           tree training

function [model] = TrainRF(X_categorical, X_continuous, Y, ntrees)
    % get the number of categorical variables
    cat_size = size(X_categorical,2);
    cat_indx = 1:cat_size;
    
    % mergre the variables arrays and the target vector
    X = [X_categorical X_continuous];
    XY = [X Y];
    
    % initialize cell to store all trained trees
    % first col is for MinLeafSize==1, second for MinLeafSize==5
    model = cell(ntrees,3);
    
    for n=1:ntrees
        % create a bootstraped dataset for each tree and return the
        % indexes
        [XY_boots, boots_ids] = datasample(XY,size(XY,1),1);
        % make each last col of model to contain ones
        model{n,3} = ones(1,size(XY,1));
        % then make those that are actually used for each tree training
        % zeros
        model{n,3}(boots_ids) = 0;

        % split X and Y after bootstrap is performed
        Y_boots = XY_boots(:,end);
        X_boots = XY_boots(:,1:end-1);

        % create a decision tree with either 1 or 5 MinLeafSize   
        minleaves = [1, 5];
        for m=1:length(minleaves)
            % create a decision tree with the selected variables
            % floor(srt(size(X,2))) samples are choosen at random for
            % splitting
            model{n,m} = fitctree(X_boots,Y_boots,'MinLeafSize',minleaves(m),...
                        'NumVariablesToSample',floor(sqrt(size(X_boots,2))),...
                        'MergeLeaves','off','Prune','off',...
                        'CategoricalPredictors',cat_indx);
        end
    end
end
