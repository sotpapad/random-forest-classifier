% load a dataset
D = load('Dataset.mat');
% assign arrays to X and Y variables assuning that they are separate
% if not, the target should be the last column of the matrix
X = D.data;
Y = D.target;

% specify the repetitions of the procedure to be 50 times
reps = 50;

% initialization of accuracies (2,2)
% columns contains accuracies for minLeafSize 1 and 5 respectively
% rows contains accuracies for ntrees 10 and 100 respectively
% each page contains the above; there are as many pages as reps
accs = zeros(2,2,reps);

% initialization of out-of-bag ids; keep the oob indices for all reps
oob_indexes = zeros(reps,size(X,1));
    
for r=1:reps
    % choose which variables are categorical and which are continuous
    % (see line 100)
    [X_cat,X_cont] = separate(X);
    sep = size(X_cat,2);
    
    % merge the input and target output arrays, in order to perform the split
    % in each (see line 76)
    X = [X_cat X_cont];
    XY = [X Y];
    [train_set, test_set] = random_split(XY);

    % partition the sets in inputs and outputs
    X_train_cat = train_set(:,1:sep);
    X_train_cont = train_set(:,sep+1:end-1);
    Y_train = train_set(:,end);
    X_test_cat = test_set(:,1:sep);
    X_test_cont = test_set(:,sep+1:end-1);
    Y_test = test_set(:,end);
    
    % perform training using either 10 or 100 trees
    ntrees = [10,100];
    for i=1:length(ntrees)
        % training of the Random Forest
        model = TrainRF(X_train_cat, X_train_cont, Y_train, ntrees(i));
        
        % 1. For the out-of-bag dataset accuracy estimation
%         predictions = PredictRF(model,X_train_cat,X_train_cont);
%         % keep only the out-of-bag samples (find those that are not)
%         boots_indx = find(all(predictions'==0));
%         oob_indx = setdiff(1:size(X_train_cat,1),boots_indx);
%         % substract 1 from all, to make predictions 0 or 1
%         predictions = predictions(oob_indx,:);
%         % select the target output of the out of bag samples
%         Y_target = Y_train(oob_indx);

        % 2. For the test_set dataset estimation        
        predictions = PredictRF(model,X_test_cat,X_test_cont);
        Y_target = Y_test; 
                
        % accuracy estimation of the algorithms (see line 90)
        acc = est_acc(predictions,Y_target);
        accs(i,:,r) = acc;
    end
end

% compute the average accuracies of each implementation
avacc_1_10 = mean(accs(1,1,:));
avacc_5_10 = mean(accs(1,2,:));
avacc_1_100 = mean(accs(2,1,:));
avacc_5_100 = mean(accs(2,2,:));

%% split the dataset in 70%-30% train-test partitions
%function [train_set,test_set] = random_split(A)
function [train_set,test_set] = random_split(A)
    % 70% can not always be integer, so so use round
    J = size(A,1);
    split = round(7/10*J);

    % randomly pick 70% of the samples, make them the training set and use
    % the rest as the test set
    [train_set, ids_train] = datasample(A,split,1,'Replace',false);
    ids_test = setdiff(1:J,ids_train);
    test_set = A(ids_test,:);
end

%% accuracy estimation
function [acc] = est_acc(predictions,target_output)
    % initialization of acc array with length equal to cols of predictions
    acc = zeros(1,size(predictions,2));
    for i=1:size(predictions,2)
        % compute acc comparing each predictions col with the target output
        acc(i) = length(find(predictions(:,i)==target_output))/length(target_output);
    end
end

%% separate the continuous from the categorical data
function [cat,cont] = separate(A)
    % suppose that the categorical variables are those that contain at most
    % 5 consecutive values or those containing only the logicals [0,1]
    
    % if large numbers are not considered to indicate continuous values:
    % then for each variable we find the minimum value and subsrtuct it
    % from all samples; this forces the values to start from 0
    %temp = A - min(A);
    
    % else, we just look at values ranging from 0 up to 4
    c0 = 0; c1 = 1; c01 = [0; 1]; c2 = [0 1 2]; c3 = [0 1 2 3]; c4 = [0 1 2 3 4];
    
    % then, we evaluate with the 5 values criterion all the variables and
    % take the indices of the variables (columns) 
    indx = zeros(1,size(A,2));
    for col=1:length(indx)
        var = A(:,col);     % var = temp(:,col) 
        un = unique(var);
        if isequal(un,c0) || isequal(un,c1) || isequal(un,c01)...
                || isequal(un,c2) || isequal(un,c3) || isequal(un,c4)
            indx(col)=1;
        end
    end
    cat = A(:,indx==1);
    cont = A(:,indx==0);
end
