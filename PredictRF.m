%% Random Forest Prediction Algorithm

% Input:
%       model: all neccessary values for new samples classification that
%               are computed by the TrainRF function, using the fitctree
%               built-in function
%       X_categorical: IxM array of categorical values; rows correspond to
%                       samples and columns to variables
%       X_continuous: IxK array of continuous values; rows correspond to
%                       samples and columns to variables
% Output:
%       predictions: Jx1 vector containing the predicted class for each of
%                   the input samples

function [predictions] = PredictRF(model, X_categorical, X_continuous)
    % get the total number of samples
    samples_num = size(X_categorical,1);
    
    % initialization of the predictions array
    predictions = zeros(samples_num,2);
    
    % merge the categorical and continuous variables
    X = [X_categorical X_continuous];
    
    % initialize a "votes" array (first page is MinLeafSize 1, second 5)
    votes = zeros(size(X,1),2,2);
    
    % select a model of different MinLeafSize
    for m=1:2
    
        % make predictions for each tree
        for tr=1:size(model,1)
            
            % 1. If we want the out-of-bag error
%             % the ids vectors indicates which samples were used in training
%             % of each tree
%             ids = model{tr,3};
% 
%             % search all samples
%             for s=1:size(ids,2)
%                 % pick those that are out-of-bag
%                 if ids(s)==1
%                     % get a vote for the particular sample
%                     vote = predict(model{tr,m},X(s,:));
%                     % store the vote accordingly
%                     if vote==0
%                         votes(s,1,m)= votes(s,1,m) + 1; % first col for 0
%                     elseif vote==1
%                         votes(s,2,m)= votes(s,2,m) + 1; % second col for 1
%                     end
%                 end
%             end
            
            % 2. If we want the test error
            % search all samples
            for s=1:samples_num
                % get a vote for the particular sample
                vote = predict(model{tr,m},X(s,:));
                % store the vote accordingly
                if vote==0
                    votes(s,1,m)= votes(s,1,m) + 1; % first col for 0
                elseif vote==1
                    votes(s,2,m)= votes(s,2,m) + 1; % second col for 1
                end
            end
            
        end
        % pick the majority vote as the class of the sample
        pred_ind = find( sum(votes(:,:,m),2) );
        [~,majority_vote] = max(votes(pred_ind,:,m),[],2);
        % because indeces start from 0, we need to substract 1
        predictions(pred_ind,m) = majority_vote-1;
    end
end
