Random Forest implementation in MATLAB using the built-in function "fitctree"
for classification

TrainRF: train many trees and create a Forest
PredictRF: use the trees and estimate the output either for the out-of-bag or
            for the test set
RunRF: load data, create training and test set, make a discrimination between
        categorical and numerical variables, predict accuracy of the the Forest
        
10 and 100 trees implemented for comparison

MinLeafSize of 1 and 5 are used in the fitctree function

Prunning has been set to off; changing to on (default) should increase the
accuracy

A static value for the number of variables used in leaves splitting is
implemented; a dynamical assignment of differrent values for each tree could
improve the accuracy